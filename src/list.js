import React, { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity, ScrollView } from "react-native";
import axios from "axios";

const Usuarios = ({ navigation }) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  const usersPress = (user) => {
    navigation.navigate("DetalhesDosUsúarios", { user });
  };

  return (
    <View>
        <FlatList
          data={users}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View>
              <TouchableOpacity onPress={() => usersPress(item)}>
                <Text style={{ marginBottom: 10, fontSize: 20, }}>{item.name}</Text>
              </TouchableOpacity>
              <View
                style={{ borderBottomWidth: 1, borderBottomColor: "gray" }}
              />
            </View>
          )}
        />
    </View>

  );
};

export default Usuarios;
