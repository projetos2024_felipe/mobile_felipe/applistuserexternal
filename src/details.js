import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import axios from "axios";

const Detalhes = ({ route }) => {
    const {user} = route.params;

    return(
        <View>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Nome: {user.name}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Nome de usúario: {user.username}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Email: {user.email}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Rua: {user.address.street}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Cidade: {user.address.city}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Zipcode: {user.address.zipcode}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Casa: {user.address.suite}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Telefone: {user.phone}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Site: {user.website}</Text>
            <Text style={{ marginBottom: 10, fontSize: 20, }}>Nome da companhia: {user.company.name}</Text>
        </View>
    )
}
export default Detalhes;