import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Usuarios from "./src/list";
import Detalhes from "./src/details";
import axios from "axios";



const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ListaDeUsúarios">
        <Stack.Screen  name="Lista De Usúarios" component={Usuarios}/>
        <Stack.Screen name="DetalhesDosUsúarios" component={Detalhes} options={{title:'Detalhes'}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
